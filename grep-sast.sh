#!/bin/sh
SIGNATURE='new Random'

# ANALYZER_* variables are set by the main SAST project.
# CI_PROJECT_DIR is set by the CI and used in a standalone configuration.
REPORT="${ANALYZER_ARTIFACT_DIR:-$CI_PROJECT_DIR}/gl-sast-report.json"
DIR="${ANALYZER_TARGET_DIR:-$CI_PROJECT_DIR}"

# search for signature
result=$(grep -I -n "$SIGNATURE" -r "$DIR" 2>/dev/null|head -n 1)
echo "result: $result" # DEBUG
if [ -n "$result" ]; then
  echo "Signature found"
  line=$(echo $result|head -n 1)
  path=$(echo "$line"|cut -d: -f1)
  relative_path="${path/$DIR\//}"
  line_number=$(echo "$line"|cut -d: -f2)
  code=$(echo "$line"|cut -d: -f3)
  echo $line
  cat > $REPORT <<EOS
{
  "version": "2.0",
  "vulnerabilities": [
    {
      "category": "sast",
      "name": "Predictable pseudorandom number generator",
      "message": "Predictable pseudorandom number generator",
      "description": "The use of java.util.Random is predictable",
      "cve": "$relative_path:$line_number:PREDICTABLE_RANDOM",
      "severity": "Medium",
      "confidence": "Medium",
      "location": {
        "file": "$relative_path",
        "start_line": $line_number,
        "end_line": $line_number,
        "class": "com.gitlab.security_products.tests.App",
        "method": "generateSecretToken1"
      },
      "identifiers": [
        {
          "type": "grep_sast",
          "name": "Find Security Bugs-PREDICTABLE_RANDOM",
          "value": "PREDICTABLE_RANDOM",
          "url": "https://find-sec-bugs.github.io/bugs.htm#PREDICTABLE_RANDOM"
        },
        {
          "type": "cwe",
          "name": "CWE-330",
          "value": "330",
          "url": "https://cwe.mitre.org/data/definitions/330.html"
        }
      ]
    }
  ]
}
EOS
else
  echo "Signature NOT found"
fi 
